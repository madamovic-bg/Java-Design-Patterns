# Java-Design-Patterns
This is example how to use some of most common Java design patterns

### What are Java Design Patterns
A design patterns are well-proved solution for solving the specific problem or task. Design Patterns are already defined and provides industry standard approach to solve a recurring problem, so it saves time if we sensibly use the design pattern.

### Java Design Patterns
* [Singleton](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/singleton)
* [Factory](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/factory)
* [Decorator](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/Decorator)
* [Builder](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/builder)
* [Proxy](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/proxy)
* [MVC](https://gitlab.com/madamovic-bg/Java-Design-Patterns/tree/master/src/mvc)

In soon more...

### Wiki
[Java-Design-Patterns Wiki](https://github.com/MilanNz/Java-Design-Patterns/wiki/Java-Design-Patterns)

### Contact
* github profile: [Milan Beograd](https://gitlab.com/u/madamovic-bg)
* twitter: [@Milan_404](https://twitter.com/Milan_404)