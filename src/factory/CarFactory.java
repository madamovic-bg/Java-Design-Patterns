/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author Milan
 */
public class CarFactory {
    
    // getFactory method returning required object - car
    public Car getFactory(String name){
        if(name == null)
            return null;
        
        switch(name.toLowerCase()){
            case "audi":
                return new Audi();
            case "bmw":
                return new BMW();
            case "mercedes":
                return new Mercedes();
            default:
                return null;
        }
    }
    
    
}
