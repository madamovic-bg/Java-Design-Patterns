/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author Milan
 */
public class BMW implements Car{

    @Override
    public void specification() {
        System.out.println("BMW 5");
        System.out.println("130hp");
        System.out.println("First registration: 2015");
        System.out.println("Price: 33.000e");
    }
    
}
