/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author Milan
 */
public class Mercedes implements Car{

    @Override
    public void specification() {
        System.out.println("Mercedes C");
        System.out.println("125hp");
        System.out.println("First registration: 2015");
        System.out.println("Price: 29.000e");
    }
    
}
