/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

/**
 *
 * @author Milan
 */
public class Audi implements Car{

    @Override
    public void specification() {
        System.out.println("Audi A4");
        System.out.println("120hp");
        System.out.println("First registration: 2015");
        System.out.println("Price: 22.000e");
    }
    
}
