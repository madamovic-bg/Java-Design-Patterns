/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javapatterns;

import decorator.RedCarDecorator;
import factory.Car;
import factory.CarFactory;
import builder.Meal;
import builder.MealBuilder;
import mvc.Building;
import mvc.BuildingController;
import mvc.BuildingShow;
import proxy.WebFile;
import proxy.ProxyFile;
import singleton.UserManager;

/**
 *
 * @author Milan
 */
public class JavaPatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /** Singleton pattern 
            Singleton pattern is one of the least difficult design patterns in Java. 
            This kind of design pattern goes under creational pattern as this pattern gives one of the most ideal approaches to make an object. 
            This pattern includes a single class which is dependable to make an object while verifying that just single object gets made. 
            This class gives an approach to get to its just object which can be got to straightforwardly without need to instantiate the class' object.
        */
        UserManager.getInstance().setUsername("Milan");
        System.out.println(UserManager.getInstance().getUsername());
        
        
        /** Factory pattern
            Factory pattern is one of most used design pattern in Java. 
            This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.
            In Factory pattern, we make object without uncovering the creation rationale to the customer and allude to recently made object utilizing a typical interface.
        */
        CarFactory carFactory = new CarFactory();
        
        Car carA = carFactory.getFactory("AUDI");
        Car carB = carFactory.getFactory("BMW");
        Car carC = carFactory.getFactory("Mercedes");
        
        carA.specification();
        carB.specification();
        carC.specification();
        
        
        
        /** Decorator pattern
            Decorator pattern allows a user to add new functionality to an existing object without altering its structure. 
            This type of design pattern comes under structural pattern as this pattern acts as a wrapper to existing class.
            This pattern makes a decorator class which wraps the first class and gives extra usefulness keeping class methods signature in place.
        */
        Car redCarA = new RedCarDecorator(carA);
        redCarA.specification();
        
        
        
        /** Builder pattern
            Builder pattern builds a complex object using simple objects and using a step by step approach. 
            This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.
            A Builder class builds the last object orderly. This builder is autonomous of different objects.
        */
        MealBuilder mealBuilder = new MealBuilder();
        Meal mealBurgerAndCoce = mealBuilder.prepareBurgerAndCoce();
        mealBurgerAndCoce.showItems();
        
        Meal mealBurgerNoCoce = mealBuilder.prepareBurgerNonCoce();
        mealBurgerNoCoce.showItems();
        
        
        /** Proxy pattern
            In proxy pattern, a class represents functionality of another class. This type of design pattern comes under structural pattern.
            The proxy design pattern allows you to provide an interface to other objects by creating a wrapper class as the proxy. 
            The wrapper class, which is the proxy, can add additional functionality to the object of interest without changing the object's code. 
        */
        WebFile fileImage = new ProxyFile("www.google.com/logo.png");
        WebFile fileText = new ProxyFile("ww.google.com/aboutus");
        
        fileImage.download();
        fileText.download();
        
        
        /** MVC pattern*/
        Building blockA11 = new Building("A11", "Washington 422", 3);
        BuildingShow view = new BuildingShow();
        BuildingController controller = new BuildingController(blockA11, view);
        
        // show
        controller.updateView();
        // update name
        controller.setBuildingName("A12");
        // show
        controller.updateView();
        
    }
    
  
    
}
