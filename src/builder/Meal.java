/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Milan
 */
public class Meal {
    private List<Item> items = new ArrayList<>();
    
    public void addItem(Item item){
        items.add(item);
    }
    
    public float getCost(){
        float cost = 0.0f;
        
        for(Item i : items){
            cost += i.price();
        }
        return cost;
    }
    
    public void showItems(){
        for(Item i : items){
            System.out.println("Products name : " + i.name());
            System.out.println("Packing : " + i.packing().pack());
            System.out.println("Price : " + i.price());
        }
    }
    
    
}
