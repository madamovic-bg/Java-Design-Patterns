/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Milan
 */
public class MealBuilder {
    
    
    public Meal prepareBurgerAndCoce(){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coca());
        return meal;
    }
    
    public Meal prepareBurgerNonCoce(){
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        return meal;
    }
    
}
