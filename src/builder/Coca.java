/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Milan
 */
public class Coca extends ColdDrink{

    @Override
    public float price() {
        return 10.2f;
    }

    @Override
    public String name() {
        return "Coce";
    }
    
}
