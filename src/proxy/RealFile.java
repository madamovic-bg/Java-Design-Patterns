/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

/**
 *
 * @author Milan
 */
public class RealFile implements WebFile{
    private String address;
    
    public RealFile(String address){
        this.address = address;
    }
    
    @Override
    public void download() {
        if(isAddressOk(address))
            System.out.println("downloading...");
        else
            System.out.println("Not found 404");
    }
    
    private boolean isAddressOk(String address){
        return (address.substring(0, 3).equals("www"))? true : false;
    }
    
}
