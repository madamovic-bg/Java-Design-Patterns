/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

/**
 *
 * @author Milan
 */
public class ProxyFile implements WebFile{
    private RealFile realFile;
    private String address;
    
    public ProxyFile(String address){
        this.address = address;
    }
    
    @Override
    public void download() {
        if(realFile == null)
            realFile = new RealFile(address);
        realFile.download();
    }
    
}
