/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

/**
 *
 * @author Milan
 */
public class Building {
    private String name;
    private String address;
    private int floors;
    
    
    public Building(String name, String address, int floors){
        this.name = name;
        this.address = address;
        this.floors = floors;
    }
    
    // setters
    public void setAddress(String address) {
        this.address = address;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    // getters
    public String getAddress() {
        return address;
    }

    public int getFloors() {
        return floors;
    }

    public String getName() {
        return name;
    }
    
}
