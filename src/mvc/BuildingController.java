/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

/**
 *
 * @author Milan
 */
public class BuildingController {
    private Building model;
    private BuildingShow view;
    
    public BuildingController(Building model, BuildingShow view){
        this.model = model;
        this.view = view;
    }
    
    public void setBuildingAddress(String address){
        model.setAddress(address);
    }
    
    public void setBuildingName(String name){
        model.setName(name);
    }
    
    public void setBuildingFloors(int floors){
        model.setFloors(floors);
    }
    
    public void updateView(){
        view.show(model);
    }
    
    
}
