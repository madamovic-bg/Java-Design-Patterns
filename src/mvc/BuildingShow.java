/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

/**
 *
 * @author Milan
 */
public class BuildingShow {
    
    // print
    public void show(Building building){
        System.out.println("name: " + building.getName());
        System.out.println("address: " + building.getAddress());
        System.out.println("floors number: " + building.getFloors());
    }
}
