/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Milan Adamovic 
 * 
 *- About Singleton -
 * Singleton pattern is one of the simplest design patterns in Java. 
 * This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.
 * This pattern involves a single class which is responsible to create an object while making sure that only single object gets created. 
 * This class provides a way to access its only object which can be accessed directly without need to instantiate the object of the class.
 */
public class UserManager {
    private static UserManager instance = null;
    private String username; // user's username

    // set username
    public void setUsername(String username) {    
        this.username = username;
    } 

    // get username
    public String getUsername() {
        return username;
    } 
    
    // create singleton
    public static UserManager getInstance() {
        if(instance == null)
            instance = new UserManager();
        
        return instance;
    }
    
    
    
    
}
