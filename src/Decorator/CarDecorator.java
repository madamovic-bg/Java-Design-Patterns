/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import factory.Car;

/**
 *
 * @author Milan
 */
public abstract class CarDecorator implements Car{
    protected Car carDecorator;
    
    public CarDecorator(Car carDecorator){
        this.carDecorator = carDecorator;
    }

    @Override
    public void specification() {
        carDecorator.specification();
    }
    
    
}
