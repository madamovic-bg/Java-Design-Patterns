/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import factory.Car;

/**
 *
 * @author Milan
 */
public class RedCarDecorator extends CarDecorator{

    public RedCarDecorator(Car carDecorator) {
        super(carDecorator);
    }

    @Override
    public void specification() {
        carDecorator.specification();
        paintCarInRed();
    }
    
    private void paintCarInRed(){
        System.out.println("Color: red");
    }
    
}
